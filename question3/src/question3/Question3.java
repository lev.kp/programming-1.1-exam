package question3;

import java.util.Scanner;

public class Question3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("How many words would you like to enter? ");
        int amount = scanner.nextInt();
        scanner.nextLine(); // Note: we need this extra call to nextLine to get rid of the newline in the input buffer.

        // TODO: complete this program as specified in the instructions

        String[] wordsArray = new String[amount];

        for (int i = 0; i < amount; i++) {
            System.out.printf("Enter Word %d: ", i + 1);
            wordsArray[i] = scanner.nextLine();
        }

        for (int i = wordsArray.length - 1; i >= 0 ; i--) {
            System.out.print(wordsArray[i] + " ");
        }

        int sum = 0;
        for (String s: wordsArray) {
            sum += s.length();
        }

        System.out.println("\nTotal length of all words: " + sum);
    }
}
