package question1;

import java.util.Random;
import java.util.Scanner;

public class Question1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please enter a sentence: ");
        String sentence = scanner.nextLine();

        System.out.print("Please enter how many characters to remove: ");
        int amount = scanner.nextInt();

        // TODO: complete this program as specified in the instructions

        StringBuilder modified = new StringBuilder();
        StringBuilder removed = new StringBuilder();
        Random r = new Random();

        modified.append(sentence);

        for (int i = 0; i < amount; i++) {
           // System.out.println(modified.length());
            int randomIndex = r.nextInt(modified.length());
            removed.append(modified.charAt(randomIndex));
            modified.deleteCharAt(randomIndex);
        }

        System.out.println("Modified sentence: " + modified);
        System.out.printf("Characters removed: '%s' ", removed);
    }
}
