package question2;

public class Computer {

    private String brand;
    private int ram;

    public String getBrand() {
        return brand;
    }

    public int getRam() {
        return ram;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }


    public Computer(String brand, int ram) {
        this.brand = brand;
        this.ram = ram;
    }

    public Computer() {

    }

    public boolean isAncient() {
        return ram < 4;
    }

    @Override
    public String toString() {
        return String.format("Computer's brand is \"%s\". %dGb of RAM.", brand, ram);
    }
}
